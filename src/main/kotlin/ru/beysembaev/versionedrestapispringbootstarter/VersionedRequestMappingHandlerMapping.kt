package ru.beysembaev.versionedrestapispringbootstarter

import org.springframework.beans.factory.annotation.Value
import org.springframework.web.method.HandlerMethod
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping
import org.springframework.web.util.UrlPathHelper
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletRequestWrapper

class VersionedRequestMappingHandlerMapping : RequestMappingHandlerMapping() {

    companion object {
        private val REQUEST_ATTRIBUTE_NAME = UrlPathHelper::class.qualifiedName + ".path"
    }

    @Value("\${api.versioning.prefix} ?: /api/v")
    private lateinit var apiPrefix: String
    @Volatile
    private var maxVersion: Int = -1

    override fun lookupHandlerMethod(lookupPath: String, request: HttpServletRequest): HandlerMethod? {
        if (maxVersion < 0) {
            setMaxVersion()
        }

        // No versioned REST methods found
        if (maxVersion == 0) {
            return super.lookupHandlerMethod(lookupPath, request)
        }

        val urlData = extractVersionData(lookupPath)
        if (urlData.version < 2 || urlData.version > maxVersion) {
            return super.lookupHandlerMethod(lookupPath, request)
        }

        val unmodifiedLookup = super.lookupHandlerMethod(lookupPath, request)
        if (unmodifiedLookup != null) {
            return unmodifiedLookup
        }

        var version = urlData.version - 1
        do {
            val newLookupPath = "${apiPrefix}${version}/${urlData.tail}"
            val updatedRequest = wrapRequest(newLookupPath, request)
            val method = super.lookupHandlerMethod(newLookupPath, updatedRequest)
            if (method != null) {
                return method
            }
            version = version.dec()
        } while (version > 0)

        return null
    }

    private fun setMaxVersion() {
        var maxV = 0
        handlerMethods.forEach {
            val version = it.key.patternsCondition?.patterns
                    ?.map(this::extractVersionData)
                    ?.maxOfOrNull(VersionedEndpointData::version)
                    ?: 0
            maxV = Integer.max(maxV, version)
        }

        synchronized(maxVersion) {
            maxVersion = Integer.max(maxVersion, maxV)
        }
    }

    /**
     * The request object is immutable. As we want to lookup different URIs (decrementing the version at every iteration),
     * it has to be wrapped to imitate changing its' URI
     * @param lookupPath the new lookup URI to set to the request
     * @param request the request to wrap
     */
    private fun wrapRequest(lookupPath: String, request: HttpServletRequest): HttpServletRequest {
        return object : HttpServletRequestWrapper(request) {
            override fun getRequestURI(): String {
                return lookupPath;
            }

            override fun getServletPath(): String {
                return lookupPath;
            }

            // The URI held by this attribute is used by Spring during the lookup - it gets compared against
            // the registered methods' mappings to find a matching one
            // If not overridden, it stays equal to the original request's URI, which means that the methods' mappings
            // are compared to the original request URI, not to the one with version decremented
            override fun getAttribute(name: String?): Any {
                if (name == REQUEST_ATTRIBUTE_NAME) {
                    return lookupPath
                }
                return super.getAttribute(name)
            }
        }
    }

    private fun extractVersionData(url: String?): VersionedEndpointData {
        val versionStr = url?.indexOf(apiPrefix)
        if (versionStr != 0) {
            return VersionedEndpointData()
        }

        val urlParts = url.substring(apiPrefix.length).split('/', limit = 2)
        if (urlParts.size != 2 || urlParts.any { it.isEmpty() }) {
            return VersionedEndpointData()
        }

        val version = try { urlParts[0].toInt() } catch (e: NumberFormatException) { 0 }
        if (version < 2) {
            return VersionedEndpointData()
        }

        return VersionedEndpointData(version, urlParts[1])
    }
}

class VersionedEndpointData(val version: Int = 0, val tail: String? = null)