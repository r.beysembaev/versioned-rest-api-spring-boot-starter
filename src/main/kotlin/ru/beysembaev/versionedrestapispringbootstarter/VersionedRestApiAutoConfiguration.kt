package ru.beysembaev.versionedrestapispringbootstarter

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.web.servlet.WebMvcRegistrations
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping

@Configuration
class VersionedRestApiAutoConfiguration {
    @Bean
    @ConditionalOnMissingBean
    fun webMvcRegistrationsHandlerMapping(): WebMvcRegistrations {
        return object : WebMvcRegistrations {
            override fun getRequestMappingHandlerMapping(): RequestMappingHandlerMapping {
                val handlerMapping = VersionedRequestMappingHandlerMapping()
                handlerMapping.order = 0
                handlerMapping.setRemoveSemicolonContent(false)
                return handlerMapping
            }
        }
    }
}